Django==1.8
django-extensions==1.7.4
requests==2.11.1
six==1.10.0
wsgiref==0.1.2
