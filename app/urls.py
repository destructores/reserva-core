from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
    # Examples:
    # url(r'^$', 'app.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', "reservas.views.user_login",name="login"),
    url(r'^login$', "reservas.views.user_login",name="login"),
    url(r'^logout$', "reservas.views.user_logout",name="logout"),
    url(r'^salas$', "reservas.views.salas",name="salas"),
    url(r'^salas/reservadas$', "reservas.views.mis_reservas",name="mis_reservas"),
    url(r'^salas/reservar$', "reservas.views.confirmar",name="confirmar_reserva"),
    url(r'^salas/disponibles$', "reservas.views.buscar_salas_disponibles",name="salas_disponibles"),
    url(r'^reservas/', include('reservas.urls')),
    url(r'^admin/', include(admin.site.urls)),
]
