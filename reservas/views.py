import json
import requests, datetime
from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.template import RequestContext, Context, Template
from django.template.loader import get_template
from reservas.dominio import Usuario
from reservas.models import Reserva
# Create your views here.

# root path
                
def user_login(request):
	mensaje = ""
	if request.method == 'POST':
		username = request.POST.get('username','')
		password = request.POST.get('password','')
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				request.session.set_expiry(86400) #sets the exp. value of the session 
				# traer datos con dni del service
				url = "http://localhost:53506/ReservaSalasGateway.svc/Usuarios/{0}".format(username)
				req = requests.get(url)
				vendedor = json.loads(req._content)
				if type(vendedor) is dict:
					request.session["vendedor"] = vendedor 
					request.session["Zona"] = vendedor["Zona"]
					print user
					print "*"*20
					print datetime.datetime.now()
					login(request,user)
					return redirect("reservas.views.salas")
				else:
					mensaje = vendedor
			else:
				mensaje = "usuario %s inactivo" % username
		else:
			mensaje = "Datos incorrectos"
	return render(request,"components/login.html",{"mensaje":mensaje})

def user_logout(request):
	logout(request)
	return HttpResponseRedirect("/login")

def usuarios(request):
	rest = ""
	res = requests.get()
	return HttpResponse(res_content)

# urls /reservas
@login_required(login_url='/login')
def salas(request):
	zona = request.session["Zona"]
	fecha = datetime.date.today().strftime('%Y-%m-%d')
	hora_inicio = datetime.datetime.now().strftime('%H:%M')
	hora_fin = datetime.datetime.now().strftime('%H:%M')
	url = "http://localhost:53506/ReservaSalasGateway.svc/Salas/{zona}".format(zona=zona)
	form_data = {"zona":zona,"fecha":fecha,"hora_inicio":hora_inicio,"hora_fin":hora_fin}
	return render(request,"pages/salas.html",form_data)

@login_required(login_url='/login')
def buscar_salas_disponibles(request):
	zona = request.session["Zona"]
	html = ""
	tpl_error = "<div class='alert alert-warning'> {0} <div>"
	if request.method == 'POST':
		try:
			fecha = request.POST.get("fecha")
			hora_inicio = request.POST.get("hora_inicio")
			hora_fin = request.POST.get("hora_fin")
			Reserva.objects.filter(fecha=fecha)
			print fecha
			zona = request.session["Zona"]
			url = "http://localhost:53506/ReservaSalasGateway.svc/Salas/{zona}".format(zona=zona)
			req = requests.get(url)
			salas = json.loads(req._content)
			#salas = [{"Disponible":True, "Nombre":"Nombre de sala","Capacidad":34,"Distrito":"Surco"},{"Disponible":True, "Nombre":"Nombre de sala","Capacidad":34,"Distrito":"Surco"}]
			#t = get_template("components/salas_encontradas.html")
			#c = Context({"salas":salas,"fecha":fecha,"hora_inicio":hora_inicio,"hora_fin":hora_fin})
			#html = t.render(c)
		except requests.ConnectionError as e:
			html = "El servicio de salas no esta disponibles, intentelo luego"
			print e
			return HttpResponse(tpl_error.format(html))
		except Exception as e:
			html = "Ha ocurrido un error, intentelo luego"
			print e
			return HttpResponse(tpl_error.format(html))
	print zona
	#res = requests.get("/%s" % zona)
	if type(salas) is list:
		c = RequestContext(request,{"salas":salas,"fecha":fecha,"hora_inicio":hora_inicio,"hora_fin":hora_fin})
		return render_to_response("components/salas_encontradas.html",c)
	else:
		return HttpResponse(tpl_error.format(salas))

@login_required(login_url='/login')
def confirmar(request):
	print "*"*20
	print request.POST
	print request.POST.keys
	print "*"*20
	if request.method == "POST":
		form = dict(request.POST)
		catering_solicitado = request.POST.get("catering",False)
		fecha_cadena = request.POST.get("fecha")
		hora_inicio_cadena = request.POST.get("hora_inicio")
		hora_fin_cadena = request.POST.get("hora_fin")
		if catering_solicitado:
			url_catering = "http://localhost:44841/Servicios.svc/servicios"
			payload = ",".join(form["catering"])
			r = requests.post(url_catering,json=payload)
			catering = json.loads(r._content)
			print "+"*100
			print r._content
		else:
			catering = None

		# Creamos reserva
		data = {
			"sala" : request.POST.get("salon"),
			"sala_descripcion" : request.POST.get("salon_descripcion"),
			"zona" : request.session["Zona"],
			"fecha" : fecha_cadena,#datetime.date.strptime(fecha_cadena,"%Y-%m-%d"),
			"hora_inicio" : datetime.datetime.strptime("%s %s" % (fecha_cadena,hora_inicio_cadena),"%Y-%m-%d %H:%M"),
			"hora_fin" : datetime.datetime.strptime("%s %s" % (fecha_cadena,hora_fin_cadena),"%Y-%m-%d %H:%M"),
			"vendedor" : request.user,
			"catering" : catering
		}
		print data
		reserva = Reserva(**data)
		reserva.save()
	#return render(request,"pages/salas_reservadas.html",{"reservas":reservas})
	return redirect("reservas.views.mis_reservas")

@login_required(login_url='/login')
def mis_reservas(request):
	reservas = Reserva.objects.filter(estado=True)
	reservas = Reserva.objects.filter(estado=True,vendedor__username=request.session["vendedor"]["Dni"])
	return render(request,"pages/salas_reservadas.html",{"reservas":reservas})

@login_required(login_url='/login')
def catering(request):
	service_catering = "http://localhost:52707/CateringServiceRest.svc/ServiciosCatering"
	r = requests.post(service_catering)
	return HttpResponse(r._content)

@login_required(login_url='/login')
def salas2(request):
	service_catering = "http://localhost:52707/CateringServiceRest.svc/ServiciosCatering"
	r = requests.post(service_catering)
	return HttpResponse(r._content)