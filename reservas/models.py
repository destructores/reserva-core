from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Reserva(models.Model):

	id = models.AutoField(primary_key=True, db_column='res_id')
	sala = models.IntegerField(db_column='sal_id')
	sala_descripcion = models.CharField(max_length=100,db_column='sal_descripcion')
	zona = models.CharField(db_column='zon_id',max_length=50)
	fecha = models.DateField(db_column='res_fecha')
	hora_inicio = models.DateTimeField(db_column='res_inicio')
	hora_fin = models.DateTimeField(db_column='res_fin')
	vendedor = models.ForeignKey(User,db_column='ven_id')
	catering = models.CharField(max_length=12,null=True,blank=True, db_column="cat_id")
	catering_confirmado = models.BooleanField(default=False,db_column='cat_confirmado')  
	estado = models.BooleanField(default=True,db_column='res_estado')
	fecha_creado = models.DateTimeField(auto_now=True,db_column='aud_fecha')

	class Meta:
		db_table = 'reservas'
		verbose_name = 'reserva'
		verbose_name_plural = 'reservas'
		ordering = ['-fecha_creado']

	def __unicode__(self):
		return self.sala_descripcion		