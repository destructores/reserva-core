import requests
from django.conf import settings

class Proxy(object):

	def __init__(self,service):
		self.service = service
		self.make_url()

	def make_url(self):
		url_tpl = "http://{ip}:{port}/{uri}"
		if settings.SERVICES.has_key(self.service):
			service_params = settings.SERVICES[self.service]
			url_service = url_tpl.format(**service_params)
			return url_service
		else:
			return False


