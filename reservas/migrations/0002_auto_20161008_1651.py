# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservas', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='reserva',
            name='catering_confirmado',
            field=models.BooleanField(default=False, db_column='cat_confirmado'),
        ),
        migrations.AddField(
            model_name='reserva',
            name='sala_descripcion',
            field=models.CharField(default='desct', max_length=100, db_column='sal_descripcion'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='reserva',
            name='catering',
            field=models.CharField(max_length=12, null=True, db_column='cat_id', blank=True),
        ),
    ]
