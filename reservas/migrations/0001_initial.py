# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Reserva',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, db_column='res_id')),
                ('sala', models.IntegerField(db_column='sal_id')),
                ('zona', models.IntegerField(db_column='zon_id')),
                ('fecha', models.DateField(db_column='res_fecha')),
                ('hora_inicio', models.DateTimeField(db_column='res_inicio')),
                ('hora_fin', models.DateTimeField(db_column='res_fin')),
                ('catering', models.IntegerField(null=True, db_column='cat_id', blank=True)),
                ('estado', models.BooleanField(default=True, db_column='res_estado')),
                ('fecha_creado', models.DateTimeField(auto_now=True, db_column='aud_fecha')),
                ('vendedor', models.ForeignKey(to=settings.AUTH_USER_MODEL, db_column='ven_id')),
            ],
            options={
                'ordering': ['-fecha_creado'],
                'db_table': 'reservas',
                'verbose_name': 'reserva',
                'verbose_name_plural': 'reservas',
            },
        ),
    ]
