# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservas', '0002_auto_20161008_1651'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reserva',
            name='zona',
            field=models.CharField(max_length=50, db_column='zon_id'),
        ),
    ]
