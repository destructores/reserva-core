class Usuario(object):
	def __init__(self,Dni, Nombre, Apellido, TelefonoMovil, Zona):
		self.Dni = Dni
		self.Nombre = Nombre
		self.Apellido = Apellido
		self.TelefonoMovil = TelefonoMovil
		self.Zona = Zona

	def __str__(self):
		return "%s:%s" % (self.Nombre, self.Apellido)
