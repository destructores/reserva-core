from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    # url(r'^blog/', include('blog.urls')),
    url(r'^usuarios$', 'reservas.views.usuarios', name='usuario'),
    url(r'^catering$', 'reservas.views.catering', name='catering'),
    #url(r'^(?P<slug>[-\w]+)$', 'novedades.views.get_novedad', name='noticia'),
)